# Abteilung 2 - Miniaturgolf

**Regelwerk:** [S12 - NormMiniatur](https://www.minigolfsport.de/pdf/Download/Regelwerk/s12_normungsbestimmungenminiaturgolf_2018-01.pdf)

## Bahnen
|Bahn|Name                       |Dateiname      | Abgeschlossen    |
|----|---------------------------|---------------|------------------|
|1   |Pyramiden                  |abt2_b1.svg    |:heavy_check_mark:|
|1   |Pyramiden (Spiegelbild)    |abt2_b1_s.svg  |:heavy_check_mark:|
|2   |Salto                      |abt2_b2.svg    |:heavy_check_mark:|
|2   |Salto (Spiegelbild)        |abt2_b2_s.svg  |:heavy_check_mark:|
|3   |Niere                      |abt2_b3.svg    |:heavy_check_mark:|
|3   |Niere (Spiegelbild)        |abt2_b3_s.svg  |:heavy_check_mark:|
|4   |Doppelwelle                |abt2_b4.svg    |:heavy_check_mark:|
|5   |Schleife                   |abt2_b5.svg    |:heavy_check_mark:|
|5   |Schleife (Spiegelbild)     |abt2_b5_s.svg  |:heavy_check_mark:|
|6   |Brücke                     |abt2_b6.svg    |:heavy_check_mark:|
|7   |Netz                       |abt2_b7.svg    |:heavy_check_mark:|
|8   |Töter                      |abt2_b8.svg    |:heavy_check_mark:|
|9   |Rohr                       |abt2_b9.svg    |:heavy_check_mark:|
|10  |Stäbe                      |abt2_b10.svg   |:heavy_check_mark:|
|10  |Stäbe (Spiegelbild)        |abt2_b10_s.svg |:heavy_check_mark:|
|11  |Labyrinth                  |abt2_b11.svg   |:heavy_check_mark:|
|11  |Labyrinth (Spiegelbild)    |abt2_b11_s.svg |:heavy_check_mark:|
|12  |Stumpfe Kegel              |abt2_b12.svg   |:heavy_check_mark:|
|12  |Stumpfe Kegel (Spiegelbild)|abt2_b12_S.svg |:heavy_check_mark:|
|13  |Sandkasten                 |abt2_b13.svg   |:heavy_check_mark:|
|14  |Passagen                   |abt2_b14.svg   |:heavy_check_mark:|
|15  |Mittelhügel                |abt2_b15.svg   |:heavy_check_mark:|
|16  |Vulkan                     |abt2_b16.svg   |:heavy_check_mark:|
|17  |Salzburger V               |abt2_b17.svg   |:heavy_check_mark:|
|18  |Winkel                     |abt2_b18.svg   |:heavy_check_mark:|
|18  |Winkel (Spiegelbild)       |abt2_b18_s.svg |:heavy_check_mark:|
|19  |Blitz                      |abt2_b19.svg   |:heavy_check_mark:|
|19  |Blitz (Spiegelbild)        |abt2_b19_s.svg |:heavy_check_mark:|
|20  |Geradschlag                |abt2_b20.svg   |:heavy_check_mark:|
|21  |Schräger Kreis             |abt2_b21.svg   |:heavy_check_mark:|
|22  |Hochteller                 |abt2_b22.svg   |:heavy_check_mark:|
|23  |Fenster                    |abt2_b23.svg   |:heavy_check_mark:|
|24  |Steilschräge               |abt2_b24.svg   |:heavy_check_mark:|
|25  |Banane                     |abt2_b25.svg   |:heavy_check_mark:|
|26  |Rampe                      |abt2_b26.svg   |:heavy_check_mark:|
|27  |Raute                      |abt2_b27.svg   |:heavy_check_mark:|
|28  |Zielhügel                  |abt2_b28.svg   |:heavy_check_mark:|
