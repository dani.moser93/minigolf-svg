# Minigolf-SVG
Hier werden alle **Minigolfbahnen** als Vektorgrafiken erstellt und kostenlos zur Verfügung gestellt. Die Skizzen sollen maßstabsgetreu und normiert sein.

Durch dieses Open-Source-Projekt kann jeder dabei helfen die Skizzen der Bahnen anzufertigen.


## DMV Regelwerke
|Abteilung |Name         |Regelwerk                    |
|----------|-------------|-----------------------------|
|1         |Beton        |[S11 - NormBeton](https://www.minigolfsport.de/pdf/Download/Regelwerk/s11_normungsbestimmungenbeton_2018-01.pdf)|
|2         |Miniaturgolf |[S12 - NormMiniatur](https://www.minigolfsport.de/pdf/Download/Regelwerk/s12_normungsbestimmungenminiaturgolf_2018-01.pdf)|
|3         |Cobigolf     |[S15 - NormMOS-C](https://www.minigolfsport.de/pdf/Download/Regelwerk/07-NormungsbestimmungenCobigolf_2017-01.pdf)|
|4         |Sterngolf    |[S16 - NormMOS-S](https://www.minigolfsport.de/pdf/Download/Regelwerk/s16_normungsbestimmungensterngolf_2011-03.pdf)|
|5         |Filzgolf     |[S13 - NormFilz](https://www.minigolfsport.de/pdf/Download/Regelwerk/s13_normungsbestimmungenfilzgolf_2016-03.pdf)|

In den [DMV Regelwerken](https://www.minigolfsport.de/download.php?subpage=11&title=DMV-Regelwerk) sind alle Bahnen genaustens beschrieben und nach dessen Anleitung die Bahnskizzen erstellt werden sollen.

## Empfehlung - [Inkscape](https://inkscape.org/de/)

Mithilfe von Inkscape kann man schnell solche Bahnen erstellen. Außerdem existieren schon einige Bahnen die man als Vorlage nutzen kann.

# Wie kann ich helfen?

Suche dir eine Bahn aus, die noch fehlt und erstelle sie mit Inkscape. Anschließend können die Skizzen dann via E-Mail laut [https://balldatenbank.web.app/footer/impressum](https://balldatenbank.web.app/footer/impressum) zugeschickt werden. Ich werde diese dann hier hinzufügen.

### Fortgeschrittene Alternative

1.  Fork the project
    
2.  Create a topic branch from  `master`.
    
3.  Make some commits to improve the project.
    
4.  Push this branch to your Gitlab project.
    
5.  Open a Pull Request on Gitlab.
    
6.  Discuss, and optionally continue committing.
    
7.  The project owner merges or closes the Pull Request.
    
8.  Sync the updated master back to your fork.