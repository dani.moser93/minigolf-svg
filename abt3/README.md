# Abteilung 3 - Cobigolf

**Regelwerk:** [S15 - NormMOS-C](https://www.minigolfsport.de/pdf/Download/Regelwerk/07-NormungsbestimmungenCobigolf_2017-01.pdf)

## Bahnen
|Bahn|Name                    |Dateiname      | Abgeschlossen    |
|----|------------------------|---------------|------------------|
|1   |Startbahn               |abt3_b1.svg    |:x:|
|2   |Käfig                   |abt3_b2.svg    |:x:|
|3   |Doppelbalken            |abt3_b3.svg    |:x:|
|4   |Tunnel                  |abt3_b4.svg    |:x:|
|5   |Brücke                  |abt3_b5.svg    |:x:|
|6   |Winkelbalken            |abt3_b6.svg    |:x:|
|7   |Hochtunnel              |abt3_b7.svg    |:x:|
|8   |Kastenpass              |abt3_b8.svg    |:x:|
|9   |Waschbecken             |abt3_b9.svg    |:x:|
|10  |Hochkrone               |abt3_b10.svg   |:x:|
|11  |Doppelwelle             |abt3_b11.svg   |:x:|
|12  |Freischlag              |abt3_b12.svg   |:x:|
|13  |Mensch ärgere Dich nicht|abt3_b13.svg   |:x:|
|14  |Hochpassage             |abt3_b14.svg   |:x:|
|15  |Treppe                  |abt3_b15.svg   |:x:|
|16  |Doppeldüse              |abt3_b16.svg   |:x:|
|17  |Germanenpott            |abt3_b17.svg   |:x:|
|18  |Hammer Bahn             |abt3_b18.svg   |:x:|